import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;


public class Main {

    private static final JButton BTN_ADD = new JButton("Add");
    private static final JButton BTN_DELETE = new JButton("Delete");
    private static final JButton BTN_UPDATE = new JButton("Update");
    private static final String[] dbList = {"-choose DB-","mySQL","PostgreSQL","MongoDB","Reddis","Cassandra","H2","GraphDB"};
    private static final JComboBox readButton = new JComboBox(dbList);       // можно вынести наверх

    //public static InfoToBeUpdated nextHer = new InfoToBeUpdated();

    //ConnectionJdbc con1;
    //PreparedStatement insert;
    public static JTable personTable;
    public static JTableModel jtm = new JTableModel();

    //public static ArrayList<Person> listOfP2 = new ArrayList<>();

    public static List<Person> listOfP2 = new ArrayList<>();



    public static void main(String[] args) {

        //нужна байда чтоб достучаться к этому листу засетать удалить

        Person p1 = new Person(1,"Geralt","theWitcher",45,"Rivia");
        Person p2 = new Person(2,"Zoltan","Heivay",12,"Rivia");
        Person p3 = new Person(3,"Marko","Pollo",32,"Kharkiv");
        Person p4 = new Person(4,"Polly","Jackson",13,"Boston");
        Person p5 = new Person(5,"Jim","Benedict",23,"Denver");
        Person p6 = new Person(6,"Hero", "ChickenChaser",34,"Albion");

        p4.setFname("Polina");

        listOfP2.add(p1);
        listOfP2.add(p2);
        listOfP2.add(p3);
        listOfP2.add(p4);
        listOfP2.add(p5);
        listOfP2.add(p6);

        //айдишники создаются внутри программы и бастанахуй!!!!

        ConnectionDb connect = new ConnectionDb("localhost","root","rootpassword");
        connect.setNameDateBasses("mysqldbpersons");
        connect.initProperties();
        connect.init();

        //создание рамки
        JFrame frame = new JFrame("Super-PersonManager-Application-3000");
        frame.setSize( new Dimension(900,600));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        //установка лейаута - расположение
        frame.setLayout(new GridBagLayout());

        //а это вообще описание/добавление кнопок

        BTN_ADD.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                AddButtonScenario addButton = new AddButtonScenario();
                //ставит наверх
                addButton.setAlwaysOnTop(true);
                //включает модалку
                addButton.setModal(true);
                //делает видимым
                addButton.setVisible(true);

            }
        });

        BTN_UPDATE.addActionListener(new ActionListener() { // и сюда же как-то передать цифру
            @Override
            public void actionPerformed(ActionEvent e) {


                int i = personTable.getSelectedRow();
                //nextHer.setToBeUpdated(new Person(listOfP2.get(i)));

                System.out.println(listOfP2.get(i).toString() + " was transferred to upd menu");


                UpdateButtonScenario updButton = new UpdateButtonScenario(listOfP2.get(i));


                //updButton.UpdateButtonScenario(listOfP2.get(i)); // ???? да ну
                updButton.setAlwaysOnTop(true);
                updButton.setModal(true);
                updButton.setVisible(true);

            }
        });
        //-------------------------------------------

        //String[] dbList = {"-choose DB-","mySQL","PostgreSQL","MongoDB","Reddis","Cassandra","H2","GraphDB"}; // можно вынести наверх этот список НЕ должен будет меняться за всю жизнь приложения
        //JComboBox readButton = new JComboBox(dbList);                                                         // можно вынести наверх

        /*
        readButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //ReadEnvironmentButton readButton = new ReadEnvironmentButton();
                if(e.getSource() == readButton){
                    System.out.println(readButton.getSelectedItem());
                }
            }
        });

         */
        //--------------------------

        // JTable
        //JTable bookTable = new JTable(jtm);
        personTable = new JTable(jtm);
        JScrollPane personTableScrollPage = new JScrollPane(personTable);
        personTableScrollPage.setPreferredSize(new Dimension(800,350));

        String [] str = new String[5];
        str[0] = "idnumber";
        str[1] = "firstName";
        str[2] = "lastName";
        str[3] = "age";
        str[4] = "address";

        //jtm.addData(str); // эта штука должна происходить при выборе в комбобокс нужного окружения
        jtm.addRowToJTable(listOfP2);


        /*
        //jtm.setValueAt(1111, 0, 0);
        p1.setFname("Ibrahim");
        System.out.println("Magic should have happend");
        jtm.fireTableDataChanged();

         */




        readButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //ReadEnvironmentButton readButton = new ReadEnvironmentButton();
                if(e.getSource() == readButton){

                    System.out.println(readButton.getSelectedItem());


                    if(readButton.getSelectedItem().equals("mySQL")){
                        System.out.println("wegotSQL");
                        //jtm.addData(connect);

                        //frame.setVisible(false);
                        //frame.repaint();
                       // personTable.repaint();
                       // personTableScrollPage.repaint();
                       // System.out.println("table repaint is done.");

                        /*
                        table.repaint();
                        scrollPane.repaint();
                        System.out.println("table repaint is done.");

                         */

                    }
                }
            }
        });








        frame.add(personTableScrollPage, new GridBagConstraints(0,0,4,1,1,1,
                GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(1,1,1,1),0,0));

        frame.add(readButton, new GridBagConstraints(0,1,1,1,1,1,
                GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(1,1,1,1),0,0));

        frame.add(BTN_ADD, new GridBagConstraints(1,1,1,1,1,1,
                GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(1,1,1,1),0,0));

        frame.add(BTN_DELETE, new GridBagConstraints(3,1,1,1,1,1,
                GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(1,1,1,1),0,0));

        frame.add(BTN_UPDATE, new GridBagConstraints(2,1,1,1,1,1,
                GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(1,1,1,1),0,0));


        frame.setVisible(true);
        frame.pack();
    }

    public Person setNewPersonToList(ArrayList<Person> list){

        return new Person();
    }


//    public void addRowToJTable()
//    {
//        DefaultTableModel model = (DefaultTableModel) jtm.getModel();
//        ArrayList<Person> list = ListUsers();
//        Object rowData[] = new Object[4];
//        for(int i = 0; i < list.size(); i++)
//        {
//            rowData[0] = list.get(i).getId();
//            rowData[1] = list.get(i).getFname();
//            rowData[2] = list.get(i).getLname();
//            rowData[3] = list.get(i).getAge();
//            rowData[3] = list.get(i).getAddress();
//            model.addRow(rowData);
//        }
//
//    }

    public ArrayList ListUsers()
    {
        ArrayList<Person> list = new ArrayList<Person>();
        Person u1 = new Person(1,"FNA","LNA",10,"AAA");
        Person u2 = new Person(2,"FNB","LNB",20,"BBB");
        Person u3 = new Person(3,"FNC","LNC",30,"CCC");
        Person u4 = new Person(4,"FND","LND",40,"DDD");
        Person u5 = new Person(5,"FNE","LNE",50,"EEE");
        Person u6 = new Person(6,"FNF","LNF",60,"FFF");
        list.add(u1);
        list.add(u2);
        list.add(u3);
        list.add(u4);
        list.add(u5);
        list.add(u6);
        return list;
    }



    /*

    private void addRecord(){
        String fname = "Marko";
        String lname = "Pollo";
        int age = 32;
        String address = "Kharkiv, Yvileinaya, 7";

        try {
            Class.forName("com.mysql.jdbc.Driver");

            con1 = DriverManager.getConnection("jdbc:mysql://localhost/mysqldbpersons","root","rootpassword");
            insert = con1.prepareStatement("insert into mysqldbpersons(fname,lname,age,address)values(?,?,?,?)");
            insert.setString(1,fname);
            insert.setString(2,lname);
            insert.setInt(3,age);
            insert.setString(4,address);
            insert.executeUpdate();

            //JOptionPane.showMessageDialog(this,"RecordAdded");

        }catch (ClassNotFoundException ex){
            //Logger.getLogger(reg.class.getName()).log(Level.SEVERE,null,ex);
            ex.printStackTrace();
        }

        catch (SQLException ex){
            //Logger.getLogger(reg.class.getName()).log(Level.SEVERE,null,ex);
            ex.printStackTrace();
        }
    }

    private void table_update()
    {
        int c;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con1 = DriverManager.getConnection("jdbc:mysql://localhost/mysqldbpersons","root","rootpassword");
            insert  = con1.prepareStatement("select * from mysqldbpersons");
            ResultSet rs = insert.executeQuery();

            ResultSetMetaData Rss = rs.getMetaData();
            c = Rss.getColumnCount();
            //DefaultTableModel DFT = (DefaultTableModel) bookTable беда байда


        }catch(Exception e){
            e.printStackTrace();
        }
    }

     */


}
