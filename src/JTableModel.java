import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JTableModel extends AbstractTableModel {

   // private static final long serialVersionUID = 3883320516766381209L;

    private int columnCount = 5;
    private ArrayList<String []> dataArrayList;

    JTableModel() {
        dataArrayList = new ArrayList<>();
        for (int i = 0; i <dataArrayList.size() ; i++) {
            dataArrayList.add(new String[getColumnCount()]);

        }
    }

    @Override
    public int getRowCount() {
        return dataArrayList.size();
    }

    @Override
    public int getColumnCount() {
        return columnCount;
    }

    @Override
    public String getColumnName(int columnIndex) {
        //return super.getColumnName(columnIndex);
        switch(columnIndex){
            case 0: return "#id";
            case 1: return "fname";
            case 2: return "lname";
            case 3: return "age";
            case 4: return "address";
        }
        return "";
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String[] rows = dataArrayList.get(rowIndex);
        return rows[columnIndex];
    }

    public void addData(String[]row){
        String [] rowTable = new String[getColumnCount()];
        rowTable = row;
        dataArrayList.add(rowTable);
    }

    //метод что добавляет лист персон в Jtable

    public void addRowToJTable(List<Person> list)
    {
        String rowData[][] = new String[list.size()][5];
        for (int i = 0; i < list.size() ; i++) {
                rowData[i][0] = String.valueOf(list.get(i).getId());
                rowData[i][1] = list.get(i).getFname();
                rowData[i][2] = list.get(i).getLname();
                rowData[i][3] = String.valueOf(list.get(i).getAge());
                rowData[i][4] = list.get(i).getAddress();
                dataArrayList.add(rowData[i]);
        }
    }



    public void addData (ConnectionDb connect){
        ResultSet result = connect.resultSetQuery("SELECT * FROM sqldbpersons ");
        try{
            while (result.next()){
                String[] row = {
                        String.valueOf(result.getInt("pid")),
                        result.getString("fname"),
                        result.getString("lname"),
                        String.valueOf(result.getInt("age")),
                        result.getString("address")
                };
                addData(row);
            }
            result.close();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}
