import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class UpdateButtonScenario extends JDialog {

    public Person cachedForUpdate = new Person(444,"Ivan","Petrovich", 55,"Gorlovka");

    private static final JButton BTN_OK = new JButton("Ok");
    private JButton BTN_CANCEL = new JButton("Cancel");

    private JTextField inputFNameField = new JTextField("firstName");
    private JLabel fNameLabel = new JLabel("Input first name:");
    private JLabel lNameLabel = new JLabel("Input last name");
    private JTextField inputLNameField = new JTextField("lastName");
    private JLabel ageLabel = new JLabel("Input age ");
    private JTextField inputAgeField = new JTextField("age");
    private JLabel addressLabel = new JLabel("Input address");
    private JTextField inputAddressField = new JTextField("address");
    private boolean updateInProcess = true;


    public UpdateButtonScenario(Person cachedForUpdate){
        //вся эта байда это просто построение окна

        System.out.println(cachedForUpdate +" person received in upd menu");
        inputFNameField.setText(cachedForUpdate.getFname());
        inputLNameField.setText(cachedForUpdate.getLname());
        inputAgeField.setText(String.valueOf(cachedForUpdate.getAge()));
        inputAddressField.setText(cachedForUpdate.getAddress());
        this.setTitle("Update parameters");
        this.setBounds(300,300,350,150);
        Container container = this.getContentPane() ;
        container.setLayout(new GridLayout(5,2,3,3)); //
        container.add(fNameLabel);
        container.add(inputFNameField);
        ButtonGroup group = new ButtonGroup();
        group.add(BTN_OK);
        group.add(BTN_CANCEL);
        BTN_OK.addActionListener(new OkButtonEventListener());

        // эту тему надо обобщить и вынести в отдельное место может и кнопки может и ивентСлушатели
        BTN_CANCEL.addActionListener(new CancelButtonEventListener()); // а то всё это параша
        container.add(lNameLabel);
        container.add(inputLNameField);
        container.add(ageLabel);
        container.add(inputAgeField);
        container.add(addressLabel);
        container.add(inputAddressField);
        container.add(BTN_OK);
        container.add(BTN_CANCEL);

//        if(updateInProcess == true){
//
//            cachedForUpdate.setFname(inputFNameField.getText());
//            cachedForUpdate.setLname(inputLNameField.getText());
//            cachedForUpdate.setAge(Integer.parseInt(inputAgeField.getText()));
//            cachedForUpdate.setAddress(inputAddressField.getText());
//
//            // должен вернуть ресетнутную персону
//
//            System.out.println(cachedForUpdate.toString() + " new year new me");
//
//            return cachedForUpdate;
//        }
//
//        System.out.println(cachedForUpdate.toString() + " returning without Update");
//        return cachedForUpdate;


    }

    class OkButtonEventListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String message = "";
//            String fname = inputFNameField.getText();
//            String lname = inputLNameField.getText();
//            String age = inputAgeField.getText();
//            String address = inputFNameField.getText();
            message += inputFNameField.getText() + " " ;
            message += inputLNameField.getText() + " " ;
            message += inputAgeField.getText() + " " ;
            message += inputAddressField.getText() + "\n" ;
            message += " record was updated ";
            JOptionPane.showMessageDialog(null,message,"Output",JOptionPane.PLAIN_MESSAGE);
            updateInProcess = true;
            UpdateButtonScenario.this.setVisible(false);
            //а теперь как эту хероту вернуть?
            //
            //надо отсюда ресетнуть запись в аррейлисте в мейне

            //улетает в лист и в БД - осталось придумать как

        }
    }

    class CancelButtonEventListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("Cancel button was pressed UPD menu");
            UpdateButtonScenario.this.setVisible(false);
            //но это не отменяет модалку? ДА! таки отменяет



        }
    }
}
