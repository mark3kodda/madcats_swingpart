import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class AddButtonScenario extends JDialog {

    PreparedStatement pst;
    Connection con;


    private static final JButton BTN_OK = new JButton("Ok");
    private JButton BTN_CANCEL = new JButton("Cancel");
    private static JTextField inputFNameField = new JTextField("firstName");
    private JLabel fNameLabel = new JLabel("Input first name:");
    private JLabel lNameLabel = new JLabel("Input last name");
    private static JTextField inputLNameField = new JTextField("lastName");
    private JLabel ageLabel = new JLabel("Input age ");
    private static JTextField inputAgeField = new JTextField("age");
    private JLabel addressLabel = new JLabel("Input address");
    private static JTextField inputAddressField = new JTextField("address");

    public AddButtonScenario(){
        //super("Add parameters");
        this.setTitle("Add parameters");
        this.setBounds(300,300,350,150);
        //this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // но эта фигня должна происходить И по принятию И по отмене

        Container container = this.getContentPane() ;
        container.setLayout(new GridLayout(5,2,3,3)); //
        // эту тему можно использовать если сначала понять расположение
        container.add(fNameLabel);
        container.add(inputFNameField);

        ButtonGroup group = new ButtonGroup();
        group.add(BTN_OK);
        group.add(BTN_CANCEL);
        BTN_OK.addActionListener(new OkButtonEventListener ()); //вызывает любой указанный класс, который
        // мы положим  втриггер добавления
        BTN_CANCEL.addActionListener(new CancelButtonEventListener());

        container.add(lNameLabel);
        container.add(inputLNameField);

        container.add(ageLabel);
        container.add(inputAgeField);

        container.add(addressLabel);
        container.add(inputAddressField);

        container.add(BTN_OK);
        container.add(BTN_CANCEL);

    }

    class CancelButtonEventListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("Cancel button was pressed ADD menu");
            AddButtonScenario.this.setVisible(false);


            //эту штуку надо как-то вынести в отдельное место
            //но это не отменяет модалку? ДА! таки отменяет



        }
    }

    static class OkButtonEventListener implements ActionListener{
        public void actionPerformed(ActionEvent e){

            String message = "";

            /*
            message += "OK-Button was pressed\n";
            message += "First name is " + inputFNameField.getText() + "\n";
            message += "Last name is " + inputLNameField.getText() + "\n";
             */

            String fname = inputFNameField.getText();
            String lname = inputLNameField.getText();
            String age = inputAgeField.getText();
            String address = inputFNameField.getText();
//
//            try{
//
//                pst = con.prepareStatement("insert into sqldbpersons(fname,lname,age,address)values(?,?,?,?)");
//                pst.setString(1,fname);
//                pst.setString(2,lname);
//                pst.setInt(3,Integer.parseInt(age));
//                pst.setString(4,address);
//                pst.executeUpdate();
//                JOptionPane.showMessageDialog(null,"Record Added");
//
//            }
//            catch (SQLException e1){
//                e1.printStackTrace();
//            }

            message += inputFNameField.getText() + " " ;
            message += inputLNameField.getText() + " " ;
            message += inputAgeField.getText() + " " ;
            message += inputAddressField.getText() + "\n" ;
            message += "was added to db.";

            //создание новой персоны ///

            //куда эту персону отправлять? или где её сохранять

            JOptionPane.showMessageDialog(null,message,"Output",JOptionPane.PLAIN_MESSAGE);


            //валидация полей
            //проверки на заполнение всех полей и выскакивание окна "заполните поля", если прожали ок До заполнения всех полей
        }
    }


}
