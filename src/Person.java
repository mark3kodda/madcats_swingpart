public class Person {

    private int id;
    private String fname;
    private String lname;
    private int age;
    private String address;

    public Person() {

    }

    public Person(int id, String fname, String lname, int age,
                  String address) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.age = age;
        this.address = address;
    }

    public Person(Person p1){
        this.id = p1.id;
        this.fname = p1.fname;
        this.lname = p1.lname;
        this.age = p1.age;
        this.address = p1.address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", fname='" + fname + '\'' +
                ", lname='" + lname + '\'' +
                ", age=" + age +
                ", address='" + address + '\'' +
                '}';
    }
}
